package App;

import Data.Document;
import Data.Terms;
import Utils.MatrixOperations;
import java.util.Scanner;

public class Engine {

    private Scanner scanner = new Scanner(System.in);

    public Engine(){}

    public void engineStart(){

        Terms terms = new Terms();

        /* Creating title terms database*/
        Document D1 = new Document("Bazy relacyjne, Bazy tekstowe, Bazy inne.");;
        D1.addTitleToTermsDatabase(terms);
        Document D2 = new Document("Bazy danych - przyklady, zastosowania. ");
        D2.addTitleToTermsDatabase(terms);
        Document D3 = new Document("Bazy danych: zalety; bazy danych: wady.");
        D3.addTitleToTermsDatabase(terms);
        Document D4 = new Document("Skladowanie danych.");
        D4.addTitleToTermsDatabase(terms);

        /* Counting IDFs */
        terms.countTermsIdf();

        /* Calculations */
        D1.makeAllCountings(terms);
        D2.makeAllCountings(terms);
        D3.makeAllCountings(terms);
        D4.makeAllCountings(terms);

        /* Displaying the results */
        System.out.println("\n"+D1.toString()+"\n");
        System.out.println(D2.toString()+"\n");
        System.out.println(D3.toString()+"\n");
        System.out.println(D4.toString()+"\n");

        System.out.println("Terms database : " + terms.toString()+"\n\n");

        /* Loading data from user */
        System.out.print("Podaj tytuł : ");
        Document D5 = new Document(scanner.nextLine());
        D5.makeAllCountings(terms);
        System.out.println(D5.toString()+"\n\n");

        /* Counting vector angles TDM */
        System.out.println("Ranking for : " + D5.getTitle() + "\n");
        System.out.println("TDM");
        System.out.println("D1 : " + MatrixOperations.countVectorsAngleTDM(D1,D5,terms.getTermNames().size()));
        System.out.println("D2 : " + MatrixOperations.countVectorsAngleTDM(D2,D5,terms.getTermNames().size()));
        System.out.println("D3 : " + MatrixOperations.countVectorsAngleTDM(D3,D5,terms.getTermNames().size()));
        System.out.println("D4 : " + MatrixOperations.countVectorsAngleTDM(D4,D5,terms.getTermNames().size()));

        /* Counting vector angles TFTDM */
        System.out.println("\nTFTDM");
        System.out.println("D1 : " + MatrixOperations.countVectorsAngleTFTDM(D1,D5,terms.getTermNames().size()));
        System.out.println("D2 : " + MatrixOperations.countVectorsAngleTFTDM(D2,D5,terms.getTermNames().size()));
        System.out.println("D3 : " + MatrixOperations.countVectorsAngleTFTDM(D3,D5,terms.getTermNames().size()));
        System.out.println("D4 : " + MatrixOperations.countVectorsAngleTFTDM(D4,D5,terms.getTermNames().size()));

        /* Counting vector angles TFIDFTDM */
        System.out.println("\nTFIDFTDM");
        System.out.println("D1 : " + MatrixOperations.countVectorsAngleTFIDFTDM(D1,D5,terms.getTermNames().size()));
        System.out.println("D2 : " + MatrixOperations.countVectorsAngleTFIDFTDM(D2,D5,terms.getTermNames().size()));
        System.out.println("D3 : " + MatrixOperations.countVectorsAngleTFIDFTDM(D3,D5,terms.getTermNames().size()));
        System.out.println("D4 : " + MatrixOperations.countVectorsAngleTFIDFTDM(D4,D5,terms.getTermNames().size()));

        scanner.close();

    }
}
