package Data;

public class Term {
    private String termName;
    private double termIdf;

    public Term(String termName) {
        this.termName = termName;
    }

    public String getTermName() {
        return termName;
    }

    public void setTermName(String termName) {
        this.termName = termName;
    }

    public double getTermIdf() {
        return termIdf;
    }

    public void setTermIdf(double termIdf) {
        this.termIdf = termIdf;
    }

    @Override
    public String toString() {
        return "\n" + '\'' + termName + '\'' +
                "  IDF = " + termIdf;
    }
}
