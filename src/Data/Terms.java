package Data;

import Utils.MathOperations;

import java.util.ArrayList;
import java.util.Arrays;

public class Terms {

    private ArrayList<Document> documents;
    private ArrayList<Term> terms;
    private double documentCount;

    public Terms() {
        this.documents = new ArrayList<>();
        this.terms = new ArrayList<>();
        this.documentCount = 0;
    }

    public ArrayList<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(ArrayList<Document> documents) {
        this.documents = documents;
    }

    public ArrayList<Term> getTerms() {
        return terms;
    }

    public void setTerms(ArrayList<Term> terms) {
        this.terms = terms;
    }

    public double getDocumentCount() {
        return documentCount;
    }

    public void addDocument(Document document){
        this.documents.add(document);
        this.documentCount++;
    }

    public void addTerm(Term term){
        this.terms.add(term);
    }

    public ArrayList<String> getTermNames(){
        ArrayList<String> termNames = new ArrayList<>();
        for(Term t:terms){
            termNames.add(t.getTermName());
        }
        return termNames;
    }

    public void countTermsIdf(){
        double result = 0;
        boolean present;
        for(Term t:terms){
            result = 0;
            for(Document d:documents){
                present = false;
                for(String s:d.getTitleTerms()){
                    if(t.getTermName().equals(s)){
                        present = true;
                    }
                }
                if(present){
                    result++;
                }
            }
            t.setTermIdf(MathOperations.log2(documentCount / result));
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(terms.toArray());
    }
}
