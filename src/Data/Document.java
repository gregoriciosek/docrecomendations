package Data;

import Utils.MatrixOperations;
import Utils.TermsGenerator;
import java.util.ArrayList;
import java.util.Arrays;

public class Document {

    private String title;
    private ArrayList<String> titleTerms;
    private ArrayList<Double> tdmMatrix;
    private ArrayList<Double> tfTdmMatrix;
    private ArrayList<Double> tfIdfTdmMatrix;
    private ArrayList<Double> normTdmMatrix;
    private ArrayList<Double> normTfTdmMatrix;
    private ArrayList<Double> normtfIdfTdmMatrix;
    private double vectorLength;

    public Document(String title){
        this.title = title;
        this.titleTerms = TermsGenerator.generateTitleTerms(title);
    }


    public String getTitle() {
        return title;
    }

    public ArrayList<String> getTitleTerms() {
        return titleTerms;
    }

    public ArrayList<Double> getTfTdmMatrix() {
        return tfTdmMatrix;
    }

    public ArrayList<Double> getNormTfTdmMatrix() {
        return normTfTdmMatrix;
    }

    public ArrayList<Double> getTfIdfTdmMatrix() {
        return tfIdfTdmMatrix;
    }

    public ArrayList<Double> getNormtfIdfTdmMatrix() {
        return normtfIdfTdmMatrix;
    }

    public ArrayList<Double> getTdmMatrix() {
        return tdmMatrix;
    }

    public ArrayList<Double> getNormTdmMatrix() {
        return normTdmMatrix;
    }

    public double getVectorLength() {
        return vectorLength;
    }

    public void addTitleToTermsDatabase(Terms terms){
        TermsGenerator.generateTerms(this.titleTerms, terms);
        terms.addDocument(this);
    }

    public void generateAndNormalizeTdmMatrix(ArrayList <String> terms){
        this.tdmMatrix = MatrixOperations.generateMatrixTDM(this.titleTerms, terms);
        this.normTdmMatrix = MatrixOperations.normalizeMatrix(this.tdmMatrix);
    }

    public void generateAndNormalizeTfTdmMatrix(ArrayList <String> terms){
        this.tfTdmMatrix = MatrixOperations.generateMatrixTFTDM(this.titleTerms, terms);
        this.normTfTdmMatrix = MatrixOperations.normalizeMatrix(this.tfTdmMatrix);
    }

    public void generateAndNormalizeTfIdfTdmMatrix(Terms terms){
        this.tfIdfTdmMatrix = MatrixOperations.generateMatrixTFIDFTDM(this.titleTerms, terms);
        this.normtfIdfTdmMatrix = MatrixOperations.normalizeMatrix(this.tfIdfTdmMatrix);
    }


    public void countVectorLength(){
        this.vectorLength = MatrixOperations.countVectorLength(this.normTdmMatrix);
    }

    public void makeAllCountings(Terms terms){
        this.generateAndNormalizeTdmMatrix(terms.getTermNames());
        this.generateAndNormalizeTfTdmMatrix(terms.getTermNames());
        this.generateAndNormalizeTfIdfTdmMatrix(terms);
        this.countVectorLength();
    }


    @Override
    public String toString() {
        return "Document title - "+ title + ".\n" +
                "\n Title terms = " + Arrays.toString(titleTerms.toArray()) +
                "\n TDM = " + Arrays.toString(tdmMatrix.toArray()) +
                "\n Norm TDM = " + Arrays.toString(normTdmMatrix.toArray()) +
                "\n TFTDM = " + Arrays.toString(tfTdmMatrix.toArray()) +
                "\n Norm TFTDM =" + Arrays.toString(normTfTdmMatrix.toArray()) +
                "\n TFIDFTDM = " + Arrays.toString(tfIdfTdmMatrix.toArray()) +
                "\n Norm TFIDFTDM = " + Arrays.toString(normtfIdfTdmMatrix.toArray()) +
                "\n Vector's Length = " + vectorLength + "\n" +
                "";
    }
}
