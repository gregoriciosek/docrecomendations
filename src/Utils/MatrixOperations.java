package Utils;

import Data.Document;
import Data.Term;
import Data.Terms;

import java.util.ArrayList;

public class MatrixOperations {

    public static ArrayList<Double> normalizeMatrix(ArrayList <Double> dx){
        ArrayList <Double> dmx = new ArrayList<>();
        double normFactor = normalizeFactor(dx);
        for(double d:dx){
            dmx.add(d*normFactor);
        }
        return dmx;
    }

    public static double normalizeFactor(ArrayList <Double> dx){
        return (1/Math.sqrt(squareSum(dx)));
    }

    public static double squareSum(ArrayList <Double> dx){
        double sqSum = 0;
        for(double d : dx){
            sqSum += d*d;
        }
        return sqSum;
    }

    public static ArrayList<Double> generateMatrixTDM(ArrayList<String> titleTerms, ArrayList<String> terms){
        ArrayList<Double> dx = new ArrayList<>();
        for(String s:terms){
            boolean present = false;
            for(String ss:titleTerms){
                if(s.equals(ss))
                    present = true;
            }
            if(present)
                dx.add((double) 1);
            else
                dx.add((double) 0);
        }
        return dx;
    }

    public static ArrayList<Double> generateMatrixTFTDM(ArrayList<String> titleTerms, ArrayList<String> terms){
        ArrayList<Double> dx = new ArrayList<>();
        double result = 0;
        for(String s:terms){
            for(String ss:titleTerms){
                if(s.equals(ss))
                    result++;
            }
            dx.add(result);
            result = 0;
        }
        return dx;
    }

    public static ArrayList<Double> generateMatrixTFIDFTDM(ArrayList<String> titleTerms, Terms terms){
        ArrayList<Double> dx = new ArrayList<>();
        double result = 0;
        for(Term t:terms.getTerms()){
            for(String ss:titleTerms){
                if(t.getTermName().equals(ss)){
                    result++;
                }
            }
            dx.add(result * t.getTermIdf());
            result = 0;
        }
        return dx;
    }

    public static double countVectorsAngleTDM(Document doc1, Document doc2, double termsCount){
        double sumTDM = 0;
        for(int i=0; i<termsCount;i++) {
            sumTDM += doc1.getNormTdmMatrix().get(i) * doc2.getNormTdmMatrix().get(i);
        }
        return sumTDM;
    };

    public static double countVectorsAngleTFTDM(Document doc1, Document doc2, double termsCount){
        double sumTFTDM = 0;
        for(int i=0; i<termsCount;i++) {
            sumTFTDM += doc1.getNormTfTdmMatrix().get(i) * doc2.getNormTfTdmMatrix().get(i);
        }
        return sumTFTDM;
    }

    public static double countVectorsAngleTFIDFTDM(Document doc1, Document doc2, double termsCount){
        double sumTFIDFTDM = 0;
        for(int i=0; i<termsCount;i++) {
            sumTFIDFTDM += doc1.getNormtfIdfTdmMatrix().get(i) * doc2.getNormtfIdfTdmMatrix().get(i);
        }
        return sumTFIDFTDM;
    }

    public static double countVectorLength(ArrayList<Double> dxm){
        return Math.sqrt(squareSum(dxm));
    }

}
