package Utils;

public class MathOperations {

    public static double log2(double n){
        double result = Math.log(n) / Math.log(2);
        return result;
    }
}
