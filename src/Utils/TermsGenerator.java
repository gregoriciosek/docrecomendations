package Utils;

import Data.Term;
import Data.Terms;

import java.util.ArrayList;

public class TermsGenerator {

    public static ArrayList<String> generateTitleTerms(String title){
        ArrayList<String> titleTerms = new ArrayList<>();
        title = StringOperations.removeSpecialCharacters(title);
        title = title.toLowerCase();
        String[] result = title.split("\\s+");

        for(String s:result){
            titleTerms.add(s);
        }
        return titleTerms;
    }

    public static void generateTerms(ArrayList<String>titleTerms, Terms terms){

        for(String s:titleTerms){
            boolean sEqualsSs = false;
            ArrayList<String> termNames = terms.getTermNames();
            for(String ss:termNames){
                if(s.equals(ss)){
                    sEqualsSs = true;
                }
            }

            if(!sEqualsSs){
                terms.addTerm(new Term(s));
            }
        }

    }

}
