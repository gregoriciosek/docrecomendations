/*

1. Utworzenie binarnej macierzy TDM
2. Utworzenie znormalizowanej macierzy TDM
3. Wydanie zapytania do macierzy TDM

*/

import App.Engine;

public class Main {

    public static void main(String  args []){
        Engine engine = new Engine();
        engine.engineStart();
    }

}
